import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios';
import { apiHost } from './config'


axios.defaults.baseURL = apiHost


Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')