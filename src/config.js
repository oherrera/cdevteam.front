let baseUrl = '';

if (process.env.NODE_ENV === 'production') {
    baseUrl = 'https://localhost:44325/api/'
} else {
    baseUrl = 'https://localhost:44325/api/'
}

export const apiHost = baseUrl;